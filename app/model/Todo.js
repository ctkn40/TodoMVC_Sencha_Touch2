// Set up a model to use in our Store
Ext.define('toDoMVCSencha.model.Todo', {
    extend: "Ext.data.Model",
    config: {
        fields: [
            {
                name: 'id'
            }, {
                name: 'title',
                type: 'string'
            }, {
                name: 'completed',
                type: 'boolean'
            }, {
                name: 'editing',
                type: 'boolean',
                defaultValue: false,
                persist: false
            }
        ]
    }
});
