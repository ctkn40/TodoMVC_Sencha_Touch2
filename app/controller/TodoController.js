Ext.define('toDoMVCSencha.controller.TodoController', {
    extend: 'Ext.app.Controller',
    CurrentTodo: null,
    config: {
        refs: {
        },
        control: {
        }
    },
    addTodo: function (title) {
        var newTodo;
        title = title.trim();

        if (title.length) {
            newTodo = Ext.create('toDoMVCSencha.model.Todo', {
                title: Ext.util.Format.htmlEncode(title),
                completed: false
            });

            toDoMVCSencha.app.TodoStore.add(newTodo);
        }

        Ext.DomQuery.selectNode('#new-todo').focus();
    },
    toggleCompleted: function (todo) {
        todo.set('completed', !todo.get('completed'));
    },
    deleteTodo: function (todo) {
        toDoMVCSencha.app.TodoStore.remove(todo);
    },
    setCurrentTodo: function (todo) {
        this.CurrentTodo = todo;
    },
    getCurrentTodo: function () {
        return this.CurrentTodo;
    },
    updateTodo: function (todo, title) {
        this.setCurrentTodo(null);

        if ((todo === null) || (todo === undefined)) {
            return;
        }

        todo.set('editing', false);
        title = title.trim();

        if (((title != null) && (title != undefined) && title.length)) {
            todo.set('title', Ext.util.Format.htmlEncode(title));
        } else {
            this.deleteTodo(todo);
        }

        Ext.DomQuery.selectNode('.newtodo ').focus();
    },
    completedCount: function () {
        return toDoMVCSencha.app.TodoStore.completedCount();
    },
    incompleteCount: function () {
        return toDoMVCSencha.app.TodoStore.incompleteCount();
    },
    areAllComplete: function () {
        return toDoMVCSencha.app.TodoStore.completedCount() === toDoMVCSencha.app.TodoStore.getCount();
    },
    onNewTodoKeyDown: function (view, e) {
        var title;
        if (e.event.keyCode === 13) {
            title = e.event.target.value.trim();
            this.addTodo(Ext.util.Format.htmlEncode(title));
            e.event.target.value = null;
            return false;
        }
        return true;
    },
    onTodoEditClick: function (view, todo, item, idx, event) {
        var editField;
        this.setCurrentTodo(todo);
        todo.set('editing', true);
        editField = Ext.DomQuery.selectNode('#todo-list li.editing .edit');
        editField.focus();
        // Ensure that focus() doesn't select all the text as well by resetting the value.
        editField.value = editField.value;
        Ext.fly(editField).on('blur', this.onTodoBlur, this);
        return false;
    },
    onTodoBlur: function (event, target) {
        Ext.fly(event.target).un('blur', this.onTodoBlur, this);
        if ((target != null) && (target != undefined)) {
            return this.updateTodo(this.getCurrentTodo(), target.value.trim());
        }
    },
    onEditTodoKeyDown: function (view, todo, item, idx, event) {
        var title;

        if (event.keyCode) {
            debugger;
            if (event.keyCode === 13) {
                if (event.target.id === 'new-todo') {
                    this.onNewTodoKeyDown(view, event);
                    return false;
                }

                title = event.target.value.trim();
                Ext.fly(event.target).un('blur', this.onTodoBlur, this);
                this.updateTodo(todo, title);
                return false;
            }
        }

        return true;
    },
    onTodoClick: function (todo, target) {

        if (Ext.fly(target).hasCls('toggle')) {
            this.toggleCompleted(todo);
        } else if (Ext.fly(target).hasCls('destroy')) {
            this.deleteTodo(todo);
        }
        return true;
    },
    onTodoToolsClick: function (view, event) {
        if (Ext.fly(event.target).hasCls('toggleall')) {
            toDoMVCSencha.app.TodoStore.toggleAllCompleted(event.target.checked);
        } else if (Ext.fly(event.target).hasCls('clearcompleted')) {
            toDoMVCSencha.app.TodoStore.deleteCompleted();
        }
        return true;
    }
});
