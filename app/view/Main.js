
console.log(window.localStorage);
Ext.define('toDoMVCSencha.view.Main', {
    extend: 'Ext.Panel',
    xtype: 'main',
    config: {
        height: '100%',
        width: '100%',
        fullscreen: true,
        layout: 'fit'
    },
    initialize: function () {
        this.callParent(arguments);

        this.MainContent = Ext.create('Ext.Panel', {
            config: {
                height: '100%',
                width: '100%',
                fullscreen: true,
                layout: 'fit'
            },
            items: [
                {
                    xtype: 'input',
                    config: {
                        id: 'new-todo',
                        placeholder: "What needs to be done?",
                        cls: 'newtodo',
                        isFocused: true,
                    },
                    onKeyUp: function (e) {
                        toDoMVCSencha.app.getController('TodoController').onNewTodoKeyDown(this, e)
                    }

                }, Ext.create('Ext.List', {
                    config: {
                        height: '100%',
                        width: '100%',
                        fullscreen: true,
                        layout: 'fit'
                    },
                    variableHeights: true,
                    scrollable: {disabled: true},
                    listeners: {
                        itemtap: function (dataview, index, item, e, element, ee, eOpts) {
                            var store = dataview.getStore(),
                                    rec = store.getAt(index);

                            toDoMVCSencha.app.getController('TodoController').onTodoClick(rec, element.target);
                        }
                    },
                    itemTpl: new Ext.XTemplate(
                            '<tpl if=\"this.getValuesLength()\">',
                            '<input id=\"toggle-all\" class=\"toggleall\" type=\"checkbox\" {[ this.areAllComplete() ? \"checked\" : \"\"]}>',
                            '<label for=\"toggle-all\">Mark all as complete<\/label>',
                            '<\/section>',
                            '<\/tpl>',
                            {
                                areAllComplete: function () {
                                    return toDoMVCSencha.app.getController('TodoController').areAllComplete();
                                },
                                incompleteCount: function () {
                                    return toDoMVCSencha.app.getController('TodoController').incompleteCount();
                                },
                                completedCount: function () {
                                    return toDoMVCSencha.app.getController('TodoController').completedCount();
                                },
                                getValuesLength: function () {
                                    if (toDoMVCSencha.app.TodoStore.getCount() > 0) {
                                        return true;
                                    }
                                    return false;
                                },
                                onTodoToolsClick: function (view, event) {
                                    return toDoMVCSencha.app.getController('TodoController').onTodoToolsClick();
                                },
                                onTodoClick: function (todo, event) {
                                    return toDoMVCSencha.app.getController('TodoController').onTodoClick(todo, event);
                                },
                                onTodoEditClick: function (view, event) {
                                    return toDoMVCSencha.app.getController('TodoController').onEditTodoKeyDown();
                                },
                                onEditTodoKeyDown: function (view, event) {
                                    return toDoMVCSencha.app.getController('TodoController').onEditTodoKeyDown();
                                }
                            })

                }), Ext.create('Ext.List', {
                    config: {
                        height: '100%',
                        width: '100%',
                        fullscreen: true,
                        layout: 'fit'
                    },
                    variableHeights: true,
                    scrollable: {disabled: true},
                    store: toDoMVCSencha.app.TodoStore,
                    listeners: {
                        itemtap: function (dataview, index, item, e, element, event, eOpts) {
                            var store = dataview.getStore(),
                                    todo = store.getAt(index);

                            if (Ext.fly(element.target).hasCls('toggle') || Ext.fly(element.target).hasCls('destroy')) {
                                toDoMVCSencha.app.getController('TodoController').onTodoClick(todo, element.target);
                            } else {
                                if (Ext.fly(element.target).hasCls('edit')) {
                                    toDoMVCSencha.app.getController('TodoController').onEditTodoKeyDown(dataview, todo, item, e, element);
                                } else {
                                    toDoMVCSencha.app.getController('TodoController').onTodoEditClick(dataview, todo, item, e, event);
                                }
                            }

                        }
                    },
                    itemTpl: new Ext.XTemplate(
                            '<tpl if=\"this.getValuesLength()\">',
                            '<section id=\"main\">',
                            '<ul id=\"todo-list\">',
                            '<tpl for=\".\">',
                            '<li class=\"todo {[ values.completed ? \"completed\" : \"\" ]} {[ values.editing ? \"editing\" : \"\" ]}\">',
                            '<div class=\"view\">',
                            '<input class=\"toggle\" type=\"checkbox\" {[ values.completed ? \"checked\" : \"\"]}>',
                            '<label>{ title }<\/label>',
                            '<button class=\"destroy\"><\/button>',
                            '<\/div>',
                            '<input class=\"edit\" value=\"{ title }\">',
                            '<\/li>',
                            '<\/tpl>',
                            '<\/ul>',
                            '<\/section>',
                            '<\/tpl>',
                            {
                                areAllComplete: function () {
                                    return toDoMVCSencha.app.getController('TodoController').areAllComplete();
                                },
                                incompleteCount: function () {
                                    return toDoMVCSencha.app.getController('TodoController').incompleteCount();
                                },
                                completedCount: function () {
                                    return toDoMVCSencha.app.getController('TodoController').completedCount();
                                },
                                getValuesLength: function () {
                                    if (toDoMVCSencha.app.TodoStore.getCount() > 0) {
                                        return true;
                                    }
                                    return false;
                                },
                                onTodoToolsClick: function (view, event) {
                                    return toDoMVCSencha.app.getController('TodoController').onTodoToolsClick();
                                },
                                onTodoClick: function (todo, event) {
                                    return toDoMVCSencha.app.getController('TodoController').onTodoClick(todo, event);
                                },
                                onTodoEditClick: function (view, event) {
                                    return toDoMVCSencha.app.getController('TodoController').onEditTodoKeyDown();
                                },
                                onEditTodoKeyDown: function (view, event) {
                                    return toDoMVCSencha.app.getController('TodoController').onEditTodoKeyDown();
                                }
                            })

                }), Ext.create('Ext.List', {
                    config: {
                        height: '100%',
                        width: '100%',
                        fullscreen: true,
                        layout: 'fit'
                    },
                    variableHeights: true,
                    scrollable: {disabled: true},
                    listeners: {
                        itemtap: function (dataview, index, item, e, element, ee, eOpts) {
                            var store = dataview.getStore(),
                                    rec = store.getAt(index);

                            toDoMVCSencha.app.getController('TodoController').onTodoClick(rec, element.target);
                        }
                    },
                    itemTpl: new Ext.XTemplate(
                            "<tpl if=\"this.completedCount()\">",
                            "<footer id=\"footer\">",
                            "<span id=\"todo-count\"><strong>{[ this.incompleteCount() ]}<\/strong> {[ ( this.incompleteCount() == 1 ) ? \"item\" : \"items\" ]} left<\/span>",
                            "<tpl if=\"this.getValuesLength()\">",
                            "<button id=\"clear-completed\" class=\"clearcompleted\">Clear completed<\/button>",
                            "<\/tpl>",
                            "<\/footer>",
                            "<\/tpl>",
                            {
                                areAllComplete: function () {
                                    return toDoMVCSencha.app.getController('TodoController').areAllComplete();
                                },
                                incompleteCount: function () {
                                    return toDoMVCSencha.app.getController('TodoController').incompleteCount();
                                },
                                completedCount: function () {
                                    debugger;
                                    return toDoMVCSencha.app.getController('TodoController').completedCount();
                                },
                                getValuesLength: function () {
                                    if (toDoMVCSencha.app.TodoStore.getCount() > 0) {
                                        return true;
                                    }
                                    return false;
                                }
                            })

                }), {
                    html:
                            "<footer id=\"info\">" +
                            "    <p>Double-click to edit a todo<\/p>" +
                            "    <p><a href=\"http:\/\/www.sencha.com\" muse_scanned=\"true\">ExtJS<\/a> with <a href=\"http:\/\/www.sencha.com\" muse_scanned=\"true\">Sencha Touch<\/a> version inspired by <a href=\"http:\/\/www.briankotek.com\" muse_scanned=\"true\">Brian Kotek<\/a><\/p>" +
                            "    <p>Part of <a href=\"http:\/\/todomvc.com\" muse_scanned=\"true\">TodoMVC<\/a><\/p>" +
                            "<\/footer>"
                }]
        });

        this.add(this.MainContent);


    }

});